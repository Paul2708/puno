unit UCard;

interface

uses
  Windows, Messages, SysUtils, Variants, Classes, Graphics, Controls, Forms,
  Dialogs, StdCtrls, ExtCtrls;

type
  TCard = class(TObject)

  color: String;
  value: Integer;
  canvas: TCanvas;
  
  brushColor: TColor;
  turned: Boolean;
  selected: Boolean;
  x, y: Integer;

  constructor Create(ColorType: String; CardValue: Integer; CanvasField: TCanvas);
  procedure Paint(xPos, yPos: Integer);
  procedure Swap(Front: Boolean);
  procedure PaintTurned(xPos, yPos, width, height: Integer);
  procedure Select(Select: Boolean);
  function IsInBorder(xMouse, yMouse: Integer) : Boolean;
  function Match(Card: TCard) : Boolean;
  function ToString: String;
  procedure PaintSingle(Pos1, Pos2, Pos3, Pos4, Pos5, Pos6, Pos7, Pos8, Pos9, X, Y: Integer);
  end;

implementation
constructor TCard.Create(ColorType: String; CardValue: Integer; CanvasField: TCanvas);
begin
  color:=ColorType;
  value:=CardValue;
  canvas:=CanvasField;

  if (color='green') then brushColor:=clLime
  else if (color='blue') then brushColor:=clBlue
  else if (color='red') then brushColor:=clRed
  else if (color='yellow') then brushColor:=clYellow;

  turned:=false;
  selected:=false;
  x:=-1;
  y:=-1;
end;

// Paint
procedure TCard.Paint(xPos, yPos: Integer);
begin
  x:=xPos;
  y:=yPos;

  with canvas do begin
    if (not turned) then begin
      // Border
      Brush.Color:=brushColor;
      Pen.Color:=clBlack;
      Rectangle(x, y, x + 80, y - 150);

      Pen.Color:=brushColor;
      Brush.Color:=clWhite;
      Rectangle(x + 5, y - 5, x + 75, y - 145);
      Brush.Color:=clGray;
      Pen.Color:=clBlack;

      if (value = 0) then begin
        Brush.Color:=clWhite;
        Pen.Color:=brushColor;
        Ellipse(x + 30, y - 65, x + 50, y - 85);
      end else begin
        Brush.Color:=brushColor;
        Pen.Color:=brushColor;
        
        case value of
          1: PaintSingle(0, 0, 0, 0, 1, 0, 0, 0, 0, x, y);
          2: PaintSingle(0, 0, 0, 1, 0, 1, 0, 0, 0, x, y);
          3: PaintSingle(0, 0, 0, 1, 1, 1, 0, 0, 0, x, y);
          4: PaintSingle(1, 0, 1, 0, 0, 0, 1, 0, 1, x, y);
          5: PaintSingle(1, 0, 1, 0, 1, 0, 1, 0, 1, x, y);
          6: PaintSingle(1, 0, 1, 1, 0, 1, 1, 0, 1, x, y);
          7: PaintSingle(1, 0, 1, 1, 1, 1, 1, 0, 1, x, y);
          8: PaintSingle(1, 1, 1, 1, 0, 1, 1, 1, 1, x, y);
          9: PaintSingle(1, 1, 1, 1, 1, 1, 1, 1, 1, x, y);
        end;
      end;
    end else begin
      // Back
      Pen.Color:=clBlack;
      Brush.Color:=clLtGray;
      Rectangle(x, y, x + 80, y - 150);
    end;
  end;
end;

// Swaps the card
procedure TCard.Swap(Front: Boolean);
begin
  turned:=not Front;
end;

// Turn the card
procedure TCard.PaintTurned(xPos, yPos, width, height: Integer);
begin
  x:=xPos;
  y:=yPos;
  
  // Back
  with canvas do begin
    Pen.Color:=clBlack;
    Brush.Color:=clLtGray;
    Rectangle(x, y, x + width, y - height);
  end;
end;

// Select the card
procedure TCard.Select(Select: Boolean);
begin
  selected:=select;
end;

// Is position in card location
function TCard.IsInBorder(xMouse, yMouse: Integer) : Boolean;
begin
  if ((xMouse > X) and (x + 80 > xMouse) and
      (yMouse < y) and (y - 150 < yMouse)) then begin
    result:=true;
    exit;
  end;

  result:=false;
end;

// Match with other card
function TCard.Match(Card: TCard) : Boolean;
begin
  if (color = Card.color) or (value = Card.value) then begin
    result:=true;
  end else begin
    result:=false;
  end;
end;

// toString
function TCard.ToString: String;
begin
  result:='TCard{color=' + color + ', value=' + IntToStr(value) + ', x=' + IntToStr(x) + ', y=' + IntToStr(y) + '}';
end;

procedure TCard.PaintSingle(Pos1, Pos2, Pos3, Pos4, Pos5, Pos6, Pos7, Pos8, Pos9, X, Y: Integer);
begin
// X=x70 Y=y+140
// Kreis = 20, Rand 5
   with canvas do begin
    if (Pos1 = 1) then Ellipse(x + (10 + 0*20), y - (45 + 0*20), x + (30 + 0*20), y - (65 + 0*20));
    if (Pos2 = 1) then Ellipse(x + (10 + 1*20), y - (45 + 0*20), x + (30 + 1*20), y - (65 + 0*20));
    if (Pos3 = 1) then Ellipse(x + (10 + 2*20), y - (45 + 0*20), x + (30 + 2*20), y - (65 + 0*20));
    if (Pos4 = 1) then Ellipse(x + (10 + 0*20), y - (45 + 1*20), x + (30 + 0*20), y - (65 + 1*20));
    if (Pos5 = 1) then Ellipse(x + (10 + 1*20), y - (45 + 1*20), x + (30 + 1*20), y - (65 + 1*20));
    if (Pos6 = 1) then Ellipse(x + (10 + 2*20), y - (45 + 1*20), x + (30 + 2*20), y - (65 + 1*20));
    if (Pos7 = 1) then Ellipse(x + (10 + 0*20), y - (45 + 2*20), x + (30 + 0*20), y - (65 + 2*20));
    if (Pos8 = 1) then Ellipse(x + (10 + 1*20), y - (45 + 2*20), x + (30 + 1*20), y - (65 + 2*20));
    if (Pos9 = 1) then Ellipse(x + (10 + 2*20), y - (45 + 2*20), x + (30 + 2*20), y - (65 + 2*20));
   end;
end;

end.
