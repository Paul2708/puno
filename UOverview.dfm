object Form1: TForm1
  Left = -8
  Top = -8
  Width = 1382
  Height = 744
  Color = clBtnFace
  Font.Charset = DEFAULT_CHARSET
  Font.Color = clWindowText
  Font.Height = -11
  Font.Name = 'Tahoma'
  Font.Style = []
  KeyPreview = True
  OldCreateOrder = False
  OnClick = FormClick
  OnCreate = FormCreate
  OnDblClick = FormDblClick
  PixelsPerInch = 96
  TextHeight = 13
  object Label1: TLabel
    Left = 0
    Top = 672
    Width = 1366
    Height = 33
    Align = alBottom
    Alignment = taCenter
    AutoSize = False
    Caption = 'Spiel wird vorbereitet...'
    Color = clWhite
    ParentColor = False
    Layout = tlCenter
  end
  object Panel1: TPanel
    Left = 0
    Top = 0
    Width = 1366
    Height = 57
    Align = alTop
    TabOrder = 0
    object Label3: TLabel
      Left = 232
      Top = 0
      Width = 109
      Height = 49
      Alignment = taCenter
      AutoSize = False
      Caption = 'Anzahl der Startkarten'
      Layout = tlCenter
    end
    object Label4: TLabel
      Left = 16
      Top = 0
      Width = 89
      Height = 49
      Alignment = taCenter
      AutoSize = False
      Caption = 'Anzahl der Spieler'
      Layout = tlCenter
    end
    object Label2: TLabel
      Left = 464
      Top = 0
      Width = 89
      Height = 49
      Alignment = taCenter
      AutoSize = False
      Caption = 'Name des Spielers'
      Layout = tlCenter
    end
    object ComboBox1: TComboBox
      Left = 112
      Top = 16
      Width = 73
      Height = 21
      ItemHeight = 13
      TabOrder = 0
      Text = '2'
    end
    object Button1: TButton
      Left = 1152
      Top = 8
      Width = 209
      Height = 41
      Caption = 'Spiel laden und starten'
      TabOrder = 1
      OnClick = Button1Click
    end
    object ComboBox3: TComboBox
      Left = 360
      Top = 16
      Width = 73
      Height = 21
      ItemHeight = 13
      TabOrder = 2
      Text = '5'
    end
    object Edit1: TEdit
      Left = 560
      Top = 16
      Width = 121
      Height = 21
      TabOrder = 3
      Text = 'Paul2708'
    end
  end
  object LB: TListBox
    Left = 1150
    Top = 57
    Width = 216
    Height = 615
    Align = alRight
    ItemHeight = 13
    TabOrder = 1
  end
  object Panel2: TPanel
    Left = 0
    Top = 57
    Width = 145
    Height = 615
    Align = alLeft
    TabOrder = 2
    object Button2: TButton
      Left = 8
      Top = 8
      Width = 129
      Height = 41
      Caption = 'Debug'
      TabOrder = 0
      OnClick = Button2Click
    end
  end
end
