unit UGame;

interface

uses
  Windows, Messages, SysUtils, Variants, Classes, Graphics, Controls, Forms,
  Dialogs, StdCtrls, ExtCtrls, UCard, UPlayer, UUtil, Contnrs;

type
  TGame = class(TObject)

  nPlayer, nCards, x, y, width, height: Integer;
  playerName: String;
  logger: TListBox;
  bar: TLabel;
  field: TCanvas;

  cardList: TObjectList;
  playerArray: array[1..4] of TPlayer;
  currentCard: TCard;
  stackCard: TCard;
  currentPlayer: TPlayer;

  timer: TTimer;

  constructor Create(Player, Cards: Integer; Name: String; ListBox: TListBox; LabelBar: TLabel;
      ClientX, ClientY, ClientWidth, ClientHeight: Integer; Canvas: TCanvas; Form: TForm);
  procedure Start;
  procedure Play(Player: TPlayer);
  procedure PlaceCard(Player: TPlayer; Card: TCard);
  procedure TakeCard(Player: TPlayer);
  procedure Log(Input: String);
  procedure DrawField;
  procedure CheckWinner;
  procedure ResumeTimer;
  procedure Cancel;
  procedure OnTimer(Sender: TObject);
  function NextPlayer : TPlayer;
  function IsCurrentPlayer(Player : TPlayer) : Boolean;
  end;

implementation
constructor TGame.Create(Player, Cards: Integer; Name: String; ListBox: TListBox; LabelBar: TLabel;
      ClientX, ClientY, ClientWidth, ClientHeight: Integer; Canvas: TCanvas; Form: TForm);
var j, i, randomIndex: Integer;
var temp: TCard;
begin
  nPlayer:=Player;
  nCards:=Cards;
  playerName:=Name;
  logger:=ListBox;
  bar:=LabelBar;
  x:=ClientX;
  y:=ClientY;
  width:=ClientWidth;
  height:=ClientHeight;
  field:=Canvas;

  // Init card stack
  cardList:=TObjectList.Create(false);
  for j:=0 to 9 do cardList.Add(TCard.Create('red', j, field));
  for j:=0 to 9 do cardList.Add(TCard.Create('green', j, field));
  for j:=0 to 9 do cardList.Add(TCard.Create('yellow', j, field));
  for j:=0 to 9 do cardList.Add(TCard.Create('blue', j, field));

  // Shuffle Cards
  for j:=0 to (cardList.Count - 1) do begin
    randomIndex:=Random(cardList.Count - 1);
    temp:=TCard(cardList.Items[randomIndex]);
    cardList.Remove(temp);
    cardList.Insert(j, temp);
  end;

  // Init player and give cards
  playerArray[1]:=TPlayer.Create(0, playerName, field);
  playerArray[2]:=TPlayer.Create(2, 'Bot Oben', field);
  playerArray[3]:=TPlayer.Create(1, 'Bot Links', field);
  playerArray[4]:=TPlayer.Create(3, 'Bot Rechts', field);

  // Give Cards
  for j:=1 to nPlayer do begin
    for i:=0 to nCards - 1 do begin
      temp:=TCard(cardList.Items[nCards]);
      if (j <> 1) then temp.Swap(false);
      playerArray[j].AddCard(temp);
      cardList.Remove(temp);
    end;
  end;
  currentCard:=TCard(cardList.Items[0]);
  stackCard:=TCard(cardList.Items[1]);

  // Init Timer
  timer:=TTimer.Create(Form);
  timer.Enabled:=false;
  timer.Interval:=2500;
  timer.OnTimer:=OnTimer;

end;

// Start game
procedure TGame.Start;
begin
  DrawField;
  Timer.Enabled:=true;

  currentPlayer:=playerArray[Random(nPlayer) + 1];

  if (IsCurrentPlayer(playerArray[1])) then bar.Caption:='Du darfst beginnen.'
  else bar.Caption:=currentPlayer.Name + ' darf beginnen.';

  Log(currentPlayer.name + ' beginnt das Spiel');
end;

// Plays the card or exit for player action
procedure TGame.Play(Player: TPlayer);
var j: Integer;
var card: TCard;
var played: Boolean;
begin
  // Check if human player
  if (IsCurrentPlayer(playerArray[1])) then begin
    Timer.Enabled:=false;
    exit;
  end;

  played:=false;
  for j:=0 to Player.cards.Count - 1 do begin
    card:=TCard(Player.cards.Items[j]);
    if (currentCard.Match(card)) then begin
      Break;
    end;

    card:=nil;
  end;

  if (Assigned(card)) then begin
    PlaceCard(currentPlayer, card);
  end else begin
    TakeCard(currentPlayer);
  end;

  currentPlayer:=NextPlayer;

  if (IsCurrentPlayer(playerArray[1])) then bar.Caption:='Du ist an der Reihe.'
  else bar.Caption:=currentPlayer.Name + ' ist an der Reihe.';

  DrawField;
end;

// Play a players card
procedure TGame.PlaceCard(Player: TPlayer; Card: TCard);
begin
  Player.cards.Remove(Card);
  currentCard:=card;
end;

// Player takes a card
procedure TGame.TakeCard(Player: TPlayer);
var card: TCard;
begin
  card:=TCard(cardList.Items[1]);
  Player.AddCard(card);
  if (IsCurrentPlayer(playerArray[1])) then card.Swap(true);
  cardList.Remove(card);
  card.x:=-1;
  card.y:=-1;

  stackCard:=TCard(cardList.Items[1]);
end;

// Log message in ListBox
procedure TGame.Log(Input: String);
begin
  logger.Items.Add(Input);
  logger.TopIndex:=logger.Items.Count - 1;
end;

// Draw game field
procedure TGame.DrawField;
var card: TCard;
var j: Integer;
var temp: TCard;
begin
  with field do begin
    // Field
    Brush.Color:=clGreen;
    Pen.Color:=clGreen;
    Rectangle(x, y, x + width, y - height);
    // Cards
    if (nPlayer >= 2) then begin
      playerArray[1].PaintCards(x, y, width, height);
      playerArray[2].PaintCards(x, y - height + 170, width, height);   
    end;
    if (nPlayer >= 3) then begin
      playerArray[3].PaintCards(x, y, width, height);
    end;
    if (nPlayer = 4) then begin
      playerArray[4].PaintCards(x + width - 180, y, width, height);
    end;
    // Current Card
    Brush.Color:=clWhite;
    Pen.Color:=clBlack;
    currentCard.Swap(true);
    currentCard.Paint(x + (width div 2) - 40, y - (height div 2) + 75);
    // Stack
    stackCard.Swap(false);
    stackCard.Paint(x + (width div 2) + 40 + 50, y - (height div 2) + 75);
    // Lines
    {Pen.Color:=clBlack;
    MoveTo(x + (width div 2), y); LineTo(x + (width div 2), y - height);
    MoveTo(x, y - (height div 2)); LineTo(x + width, y - (height div 2));}
    // Refresh
    Refresh;
  end;
end;

procedure TGame.CheckWinner;
var j: Integer;
var player: TPlayer;
begin
  for j:=1 to nPlayer do begin
    player:=playerArray[j];
    if (player.cards.Count = 0) then begin
      Timer.Enabled:=false;
      ShowMessage(player.name + ' hat die Runde gewonnen. Gl�ckwunsch!');
      bar.Caption:=(player.name + ' hat die Runde gewonnen. Gl�ckwunsch!');
      Exit;
    end;
  end;
end;

procedure TGame.ResumeTimer;
begin
  Timer.Interval:=2500;
  Timer.Enabled:=false;
  Timer.Enabled:=true;
end;

procedure TGame.Cancel;
begin
  Timer.Enabled:=false;
  bar.Caption:='Das Spiel wurde beendet.';

  with field do begin
    Brush.Color:=clGreen;
    Pen.Color:=clGreen;
    Rectangle(x, y, x + width, y - height);
  end;

  Log('Das Spiel wurde beendet');
end;

// Timer Listener
procedure TGame.OnTimer(Sender: TObject);
begin
  Play(currentPlayer);
  CheckWinner;
end;

// Get next player
function TGame.NextPlayer : TPlayer;
var index : Integer;
begin
  index:=currentPlayer.position;
  if (nPlayer = 2) then begin
    case index of
      0: index:=2;
      2: index:=1;
    end;
  end else
  if (nPlayer = 3) then begin
     case index of
      0: index:=3;
      1: index:=2;
      2: index:=1;
    end;
  end else
  if (nPlayer = 4) then begin
    case index of
      0: index:=3;
      1: index:=2;
      2: index:=4;
      3: index:=1;
    end;
  end;

  result:=playerArray[index];
end;

// Check if current player is Player
function TGame.IsCurrentPlayer(Player : TPlayer) : Boolean;
begin
  result:=(currentPlayer.name = Player.name);
end;

end.
