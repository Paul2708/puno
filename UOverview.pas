unit UOverview;

interface

uses
  Windows, Messages, SysUtils, Variants, Classes, Graphics, Controls, Forms,
  Dialogs, StdCtrls, ExtCtrls, UGame, UUtil, UCard, Math;

type
  TForm1 = class(TForm)

    Panel1: TPanel;
    LB: TListBox;
    Panel2: TPanel;
    Label1: TLabel;
    ComboBox1: TComboBox;
    Button1: TButton;
    Label3: TLabel;
    ComboBox3: TComboBox;
    Label4: TLabel;
    Edit1: TEdit;
    Button2: TButton;
    Label2: TLabel;
    
    procedure FormCreate(Sender: TObject);
    procedure Button1Click(Sender: TObject);
    procedure Button2Click(Sender: TObject);
    procedure FormClick(Sender: TObject);
    procedure FormDblClick(Sender: TObject);
  private
    Game: TGame;
  public
  end;

var
  Form1: TForm1;

implementation

{$R *.dfm}

// Form creation
procedure TForm1.FormCreate(Sender: TObject);
var j: Integer;
begin
  // Init game field
  Left:=Screen.Monitors[0].Left;
  WindowState:=wsMaximized;
  Label1.Caption:='Das Spiel wird gestartet...';
  Edit1.Text:='Paul';
  for j:=2 to 4 do ComboBox1.AddItem(IntToStr(j), TObject(j));
  ComboBox1.ItemIndex:=0;
  for j:=1 to 7 do ComboBox3.AddItem(IntToStr(j), TObject(j));
  ComboBox3.ItemIndex:=4;
  Caption:='UNO v1.0';

  // Random
  Randomize;

  LB.Items.Add('Fenster wurde geladen');
end;

// Start and canel game
procedure TForm1.Button1Click(Sender: TObject);
var name: String;
begin
  if (Button1.Caption = 'Spiel laden und starten') then begin
    // Check valid arguments
    if (ComboBox1.ItemIndex = -1) then ComboBox1.ItemIndex:=0;
    if (ComboBox3.ItemIndex = -1) then ComboBox3.ItemIndex:=4;
    name:=TUtil.FormatName(Edit1.Text);
    if (name = '') then name:='Spieler';

    // Game start
    Game:=TGame.Create(
          Integer(ComboBox1.Items.Objects[ComboBox1.ItemIndex]),
          Integer(ComboBox3.Items.Objects[ComboBox3.ItemIndex]),
          name,
          LB,
          Label1,
          Panel2.Width,
          ClientHeight - Label1.Height,
          ClientWidth - Panel2.Width - LB.Width,
          ClientHeight - Label1.Height - Panel1.Height,
          Canvas,
          Self
    );

    Game.Log('');
    Game.Log('Aktuelle Einstellungen:');
    Game.Log(' - ' + IntToStr(Game.nPlayer) + ' Spieler');
    Game.Log(' - ' + IntToStr(Game.nCards) + ' Startkarten');
    Game.Log(' - Name: ' + Game.playerName);
    Game.Log(' - Fenster: ' + IntToStr(Game.x) + '|' + IntToStr(Game.y) + ' (' +
        IntToStr(Game.width) + 'x' + IntToStr(Game.height) + ')');
    Game.Log('');
    Game.Log('Spiel wurde gestartet');

    Game.Start;

    Button1.Caption:='Spiel beenden';
  end else if (Button1.Caption = 'Spiel beenden') then begin
    // Cancel game
    if (Assigned(Game)) then begin
      Game.Cancel;
      Game.Destroy;
    end;
    Button1.Caption:='Spiel laden und starten';
  end;
end;

// Debug button
procedure TForm1.Button2Click(Sender: TObject);
begin
  ComboBox1.ItemIndex:=2;
  ComboBox3.ItemIndex:=4;
  Edit1.Text:='Paul2708';
end;

// Selcet card
procedure TForm1.FormClick(Sender: TObject);
var point: TPoint;
var card: TCard;
begin
  point:=Mouse.CursorPos;
  card:=Game.playerArray[1].GetCard(point.X, point.Y);

  // Check clicked card
  if (Assigned(card)) then begin
    Game.playerArray[1].Select(card);
  end;

  Game.DrawField;
end;

// Take or place card
procedure TForm1.FormDblClick(Sender: TObject);
var point: TPoint;
var card: TCard;
begin
  if (Game.IsCurrentPlayer(Game.playerArray[1])) then begin
    point:=Mouse.CursorPos;
    card:=Game.playerArray[1].GetCard(point.X, point.Y);
    // Check selected card
    if (Assigned(card) and card.selected) then begin
      if (Game.currentCard.Match(card)) then begin
        Game.PlaceCard(Game.playerArray[1], card);
        
        Game.currentPlayer:=Game.NextPlayer;
        Game.ResumeTimer;
        Game.bar.Caption:=Game.currentPlayer.Name + ' ist an der Reihe.';

        Game.CheckWinner;
        Game.DrawField;
      end;
    // Stack card
    end else begin
      if (Game.stackCard.IsInBorder(point.X, point.Y)) then begin
        Game.TakeCard(Game.playerArray[1]);

        Game.currentPlayer:=Game.NextPlayer;
        Game.ResumeTimer;
        Game.bar.Caption:=Game.currentPlayer.Name + ' ist an der Reihe.';

        Game.DrawField;
      end;
    end;
  end;
end;

end.
