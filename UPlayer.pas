unit UPlayer;

interface

uses
  Windows, Messages, SysUtils, Variants, Classes, Graphics, Controls, Forms,
  Dialogs, StdCtrls, ExtCtrls, UCard, Contnrs;

type
  TPlayer = class(TObject)

  position: Integer;
  name: String;
  canvas: TCanvas;

  cards: TObjectList;

  constructor Create(Location: Integer; PlayerName: String; GameCanvas: TCanvas);
  procedure PaintCards(x, y, width, height: Integer);
  procedure Select(Card: TCard);
  procedure AddCard(Card: TCard);
  function GetCard(X, Y: Integer) : TCard;
  end;

implementation
constructor TPlayer.Create(Location: Integer; PlayerName: String; GameCanvas: TCanvas);
begin
  position:=Location;
  name:=PlayerName;
  canvas:=GameCanvas;

  cards:=TObjectList.Create(false);
end;

procedure TPlayer.PaintCards(x, y, width, height: Integer);
var j: Integer;
var card: TCard;
begin
  if (cards.Count = 0) then exit;

  // Player and upper Position
  if (position = 0) or (position = 2) then begin
    if (cards.Count = 1) then begin
      TCard(cards.Items[0]).Paint(x + (width div 2) - 40, y - 10);
    end else begin
      // Mehr als 7
      if (Cards.Count > 7) then begin
         for j:=0 to 6 do begin
            TCard(cards.Items[j]).Paint(x + (width div 2) - 40 - (7 div 2)*85 + j*85, y - 10);
        end;
      // Ungerade
      end else if (Odd(Cards.Count)) then begin
        for j:=0 to cards.count - 1 do begin
          TCard(cards.Items[j]).Paint(x + (width div 2) - 40 - (cards.Count div 2)*85 + j*85, y - 10);
        end;
      // Gerade
      end else begin
        for j:=0 to (cards.Count div 2) - 1 do begin
          TCard(cards.Items[j]).Paint(x + (width div 2) - 85 - j*85, y - 10);
        end;
        for j:=0 to (cards.Count div 2) - 1 do begin
          TCard(cards.Items[j + cards.Count div 2]).Paint(x + (width div 2)+ 5+ j*85, y - 10);
        end;
      end;
    end;
  // Bot position
  end else begin
    if (cards.Count = 1) then begin
      TCard(cards.Items[0]).PaintTurned(x + 10, y - (height div 2) + 40, 150, 80);
    end else begin
      // Mehr als 7
      if (Cards.Count > 7) then begin
         for j:=0 to 6 do begin
           TCard(cards.Items[j]).PaintTurned(x + 10, y - (height div 2) + 40 + (7 div 2)*85 - j*85, 150, 80);
        end;
      // Ungerade
      end else if (Odd(cards.Count)) then begin
        for j:=0 to cards.Count - 1 do begin
          TCard(cards.Items[j]).PaintTurned(x + 10, y - (height div 2) + 40 + (cards.Count div 2)*85 - j*85, 150, 80);
        end;
      // Gerade
      end else begin
        for j:=0 to (cards.Count div 2) - 1 do begin
          TCard(cards.Items[j]).PaintTurned(x + 10, y - (height div 2) - 5 - j*85, 150, 80);
        end;
        for j:=0 to (cards.Count div 2) - 1 do begin
          TCard(cards.Items[j + cards.Count div 2]).PaintTurned(x + 10, y - (height div 2) + 85 + j*85, 150, 80);
        end;
      end;
    end;
  end;

  // Selected
  for j:=0 to cards.Count - 1 do begin
    card:=TCard(cards.Items[j]);
    if (card.selected) then begin
      with canvas do begin
        Brush.Color:=clYellow;
        Pen.Color:=clYellow;
        Rectangle(card.x - 5, card.y + 5, card.x + 85, card.y - 155);
        card.Paint(card.x, card.y);
      end;
    end;
  end;
end;

// Select a card (yellow border)
procedure TPlayer.Select(Card: TCard);
var j: Integer;
begin
  for j:=0 to cards.Count - 1 do begin
    TCard(cards.Items[j]).Select(false);
  end;

  Card.Select(true);
end;

// Add a card to the current stack
procedure TPlayer.AddCard(Card: TCard);
begin
  cards.Add(Card);
end;

// Get a card by given coords
function TPlayer.GetCard(X, Y: Integer) : TCard;
var j: Integer;
var card: TCard;
begin
  for j:=0 to cards.Count - 1 do begin
    card:=TCard(cards.Items[j]);

    if ((x > card.X) and (card.x + 80 > X) and
        (y < card.y) and (card.y - 150 < y)) then begin
        result:=card;
        exit;
    end;

    result:=nil;
  end;
end;

end.
